package com.example.demo.service;

import com.example.demo.entity.LoanApplication;
import com.example.demo.entity.LoanApplicationStatusEnum;

import java.time.LocalDate;
import java.time.ZoneId;

class LoanApplicationScorer {

    static LoanApplicationStatusEnum getStatus(double score) {
        return score > 3500 ? LoanApplicationStatusEnum.APPROVED : score < 2500 ? LoanApplicationStatusEnum.REJECTED : LoanApplicationStatusEnum.MANUAL;
    }

    static double getScore(LoanApplication loanApplication) {
        int firstNameValue = loanApplication.getFirstName() != null
                ? loanApplication.getFirstName().toLowerCase().chars()
                .reduce(0, (left, right) -> left + right - 96) : 0;
        LocalDate date = loanApplication.getBirthDate() != null
                ? loanApplication.getBirthDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate() : null;

        return firstNameValue
                + loanApplication.getSalary() * 1.5
                - loanApplication.getMonthlyLiability() * 3
                + (date != null ? date.getYear() : 0)
                - (date != null ? date.getMonthValue() : 0)
                - (date != null ? date.getDayOfYear() : 0);
    }
}

package com.example.demo.service;

import com.example.demo.entity.LoanApplication;
import com.example.demo.repository.LoanApplicationRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class LoanApplicationService {

    private final LoanApplicationRepository loanApplicationRepository;

    public LoanApplicationService(LoanApplicationRepository loanApplicationRepository) {
        this.loanApplicationRepository = loanApplicationRepository;
    }

    public List<LoanApplication> getAllApplications(String sortBy, String sortDirection) {
        return StreamSupport.stream(
                loanApplicationRepository.findAll(Sort.by(Sort.Direction.fromString(sortDirection), sortBy)).spliterator(),
                false).collect(Collectors.toList());
    }

    public LoanApplication createApplication(LoanApplication loanApplication) {
        double score = LoanApplicationScorer.getScore(loanApplication);
        loanApplication.setScore(score);
        loanApplication.setStatus(LoanApplicationScorer.getStatus(score));
        return loanApplicationRepository.save(loanApplication);
    }

    public LoanApplication updateApplication(Long id, LoanApplication loanApplication)
            throws ResourceNotFoundException {
        LoanApplication application = loanApplicationRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Loan Application not found on :: " + id));

        application.setStatus(loanApplication.getStatus());
        return loanApplicationRepository.save(loanApplication);
    }

    public void deleteApplication(Long id) throws ResourceNotFoundException {
        try {
            loanApplicationRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("Loan Application not found on :: " + id);
        }
    }

}

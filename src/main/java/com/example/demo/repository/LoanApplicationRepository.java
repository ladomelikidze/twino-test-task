package com.example.demo.repository;

import com.example.demo.entity.LoanApplication;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LoanApplicationRepository extends PagingAndSortingRepository<LoanApplication, Long> {}

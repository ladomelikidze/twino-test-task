package com.example.demo.entity;

public enum LoanApplicationStatusEnum {
    APPROVED, MANUAL, REJECTED
}

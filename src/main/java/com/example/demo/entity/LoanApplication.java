package com.example.demo.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name="LOANS")
public class LoanApplication {

    private @Id @GeneratedValue Long id;
    private String personalId;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String employer;
    private double salary;
    private double monthlyLiability;
    private double requestedAmount;
    private int requestedTerm;
    private RequestTermUnitEnum requestedTermUnit;
    private double score;
    private LoanApplicationStatusEnum status;

}

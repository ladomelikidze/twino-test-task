package com.example.demo.controller;

import com.example.demo.entity.LoanApplication;
import com.example.demo.service.LoanApplicationService;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/loan/application")
public class LoanApplicationController {

    private final LoanApplicationService loanApplicationService;

    public LoanApplicationController(LoanApplicationService loanApplicationService) {
        this.loanApplicationService = loanApplicationService;
    }

    @GetMapping
    public List<LoanApplication> getAllApplications(@RequestParam("sortBy") String sortBy,
                                                    @RequestParam("sortDirection") String sortDirection) {
        return loanApplicationService.getAllApplications(sortBy, sortDirection);
    }

    @PostMapping
    public LoanApplication createApplication(@Valid @RequestBody LoanApplication loanApplication) {
        return loanApplicationService.createApplication(loanApplication);
    }

    @PutMapping("/{id}")
    public LoanApplication updateApplication(@PathVariable(value = "id") Long id,
                                             @Valid @RequestBody LoanApplication loanApplication)
            throws ResourceNotFoundException {
        return loanApplicationService.updateApplication(id, loanApplication);
    }

    @DeleteMapping("/{id}")
    public void deleteApplication(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
        loanApplicationService.deleteApplication(id);
    }

}

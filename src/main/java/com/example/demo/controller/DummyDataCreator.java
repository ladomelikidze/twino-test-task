package com.example.demo.controller;

import com.example.demo.entity.LoanApplication;
import com.example.demo.entity.RequestTermUnitEnum;
import com.example.demo.repository.LoanApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class DummyDataCreator {

    @Autowired
    private final LoanApplicationRepository loanApplicationRepository;

    public DummyDataCreator(LoanApplicationRepository loanApplicationRepository) {
        this.loanApplicationRepository = loanApplicationRepository;
    }

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        LoanApplication app = new LoanApplication();
        app.setBirthDate(new Date());
        app.setMonthlyLiability(666);
        app.setSalary(999);
        app.setFirstName("A");
        app.setEmployer("LLC A");
        app.setLastName("AAA");
        app.setPersonalId("11111111111");
        app.setRequestedAmount(6666);
        app.setRequestedTerm(6);
        app.setRequestedTermUnit(RequestTermUnitEnum.MONTH);
        loanApplicationRepository.save(app);

        app = new LoanApplication();
        app.setBirthDate(new Date());
        app.setMonthlyLiability(6664);
        app.setSalary(9989);
        app.setFirstName("B");
        app.setEmployer("LLC B");
        app.setLastName("BBB");
        app.setPersonalId("222222222");
        app.setRequestedAmount(6444);
        app.setRequestedTerm(12);
        app.setRequestedTermUnit(RequestTermUnitEnum.DAY);
        loanApplicationRepository.save(app);
    }
}

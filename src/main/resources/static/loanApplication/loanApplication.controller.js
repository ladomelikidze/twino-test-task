var app = angular.module('BlankApp', ['ngMaterial', 'ngMessages', 'md.data.table']);
app.controller('mainCtrl', function($scope, $http) {
    $scope.selected = [];
    $scope.order = 'personalId';
    $scope.loading = true;

    $scope.loadApplications = function () {
        $scope.loading = true;
        $http({
            method: 'GET',
            url: '/api/v1/loan/application',
            params: {
                sortBy: $scope.order.startsWith('-') ? $scope.order.substring(1) : $scope.order,
                sortDirection: $scope.order.startsWith('-') ? "DESC" : "ASC"
            }
        }).then(function successCallback(response) {
            $scope.loanApplications = response.data;
            $scope.loading = false;
        }, function errorCallback(response) {
            console.log(response);
            $scope.loading = false;
        });
    };

    $scope.loadApplications();
});
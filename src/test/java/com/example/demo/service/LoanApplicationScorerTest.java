package com.example.demo.service;

import com.example.demo.entity.LoanApplication;
import com.example.demo.entity.LoanApplicationStatusEnum;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class LoanApplicationScorerTest {

    @Test
    void getStatus() {
        assert LoanApplicationScorer.getStatus(-666) == LoanApplicationStatusEnum.REJECTED;
        assert LoanApplicationScorer.getStatus(2666) == LoanApplicationStatusEnum.MANUAL;
        assert LoanApplicationScorer.getStatus(6666) == LoanApplicationStatusEnum.APPROVED;
    }

    @Test
    void getScore() {
        LoanApplication app = new LoanApplication();
        assert LoanApplicationScorer.getScore(app) == 0;
        app.setFirstName("Demo");
        assert LoanApplicationScorer.getScore(app) == 37;
        app.setSalary(6);
        assert LoanApplicationScorer.getScore(app) == 37 + 9;
        app.setMonthlyLiability(2);
        assert LoanApplicationScorer.getScore(app) == 37 + 9 - 6;
        app.setBirthDate(new Date()); // TODO
    }
}